package com.mileva.cuentacuentos.server.rest;

import com.mileva.cuentacuentos.server.business.StoryFacade;
import com.mileva.cuentacuentos.server.persistence.Story;
import com.mileva.cuentacuentos.server.persistence.StoryList;
import java.net.URI;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StoryREST {
    @Context
    private UriInfo uriInfo;
    @EJB
    private StoryFacade storyFacade;

    @POST
    @Path("stories/")
    public Response createStory(Story story) {
        if (story==null)
            throw new BadRequestException();
        storyFacade.persistEntity(story);
        URI storyUri = uriInfo.getAbsolutePathBuilder().path("stories/" + Long.toString(story.getId())).build();
        return Response.created(storyUri).build();
    }

    @GET
    @Path("stories/{id}/")
    public Response getStory(@PathParam("id") String id) {
        Story story = null;
        story = storyFacade.getStoryFindById(Long.parseLong(id.trim()));
        if (story == null)
            throw new NotFoundException();
        return Response.ok(story).build();
    }    

    @GET
    @Path("stories/")
    public Response getAllStories() {
        List<Story> storyList = storyFacade.getStoryFindAll();
        return Response.ok(new StoryList(storyList)).build();
    }    
}