package com.mileva.cuentacuentos.server.persistence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@NamedQueries({
    @NamedQuery(name="Story.findAll", query="select s from Story s"),
})
@XmlRootElement
@Table(name="T_STORY")
public class Story {
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="StoryIDGenerator")    
    private long id;
    private String category;
    private String name;
    private String content;

    public Story() {}

    public void setId(long id) {this.id=id;}
    public void setCategory(String category) {this.category=category;}
    public void setName(String name) {this.name=name;}
    public void setContent(String content) {this.content=content;}

    public long getId() {return id;}
    public String getCategory() {return category;}
    public String getName() {return name;}
    public String getContent() {return content;}

}