package com.mileva.cuentacuentos.server.persistence;

import com.mileva.cuentacuentos.server.persistence.Story;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
 
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement()
public class StoryList<T> {
    @XmlElements({
        @XmlElement(name="stories", type=Story.class),
    })
    private List<T> storyList = new ArrayList<T>();
     
    public StoryList() {}
 
    public StoryList(List<T> storyList) {
        this.storyList = storyList;
    }
 
    public List<T> getStoryList() {
        return storyList;
    }
 
    public void setStoryList(List<T> storyList) {
        this.storyList = storyList;
    }   
}