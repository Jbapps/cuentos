package com.mileva.cuentacuentos.server.business;

import com.mileva.cuentacuentos.server.persistence.Story;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class StoryFacade {
    @PersistenceContext(name="cuentacuentosdb")
    private EntityManager em;

    public <T> T persistEntity(T entity) {
        em.persist(entity);
        return entity;
    }

    public Story getStoryFindById(Long id) {
        return em.find(Story.class, id);
    }

    public List<Story> getStoryFindAll() {
        return em.createNamedQuery("Story.findAll", Story.class).getResultList();
    }
}