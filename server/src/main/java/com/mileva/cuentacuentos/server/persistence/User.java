package com.mileva.cuentacuentos.server.persistence;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="T_USER")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="UserIDGenerator")
    private long id;
    private String alias;
    @OneToMany(cascade={CascadeType.ALL}, orphanRemoval = true)
    private List<Story> stories;

    public User() {}

    public void setId(long id) {this.id=id;}
    public void setAlias(String alias) {this.alias=alias;}
    public void setStories(List<Story> stories) {
        this.stories=stories;
    }

    public long getId() {return id;}
    public String getAlias() {return alias;}
    public List<Story> getStories() {
        if (stories==null)
            stories = new LinkedList<Story>();
        return stories;
    }    

}